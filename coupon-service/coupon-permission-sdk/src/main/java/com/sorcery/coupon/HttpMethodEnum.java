package com.sorcery.coupon;

/**
 * Http方法类型枚举
 *
 * @author jinglv
 * @date 2024/4/29 16:45
 */
public enum HttpMethodEnum {
    GET,
    HEAD,
    POST,
    PUT,
    PATCH,
    DELETE,
    OPTIONS,
    TRACK,
    ALL
}
