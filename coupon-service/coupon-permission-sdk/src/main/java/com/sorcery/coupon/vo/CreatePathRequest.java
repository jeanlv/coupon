package com.sorcery.coupon.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 路径创建请求定义
 *
 * @author jinglv
 * @date 2024/4/29 16:50
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePathRequest {
    /**
     * 路径列表
     */
    private List<PathInfo> pathInfoList;

    /**
     * 定义内部类-路径信息
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class PathInfo {
        /**
         * 路径模式
         */
        private String pathPattern;
        /**
         * Http方法类型
         */
        private String HttpMethod;
        /**
         * 路径名称
         */
        private String pathName;
        /**
         * 服务名称
         */
        private String serviceName;
        /**
         * 操作模式：读、写
         */
        private String opMode;
    }
}
