package com.sorcery.coupon.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 权限校验请求对象定义
 *
 * @author jinglv
 * @date 2024/4/29 16:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckPermissionRequest {
    /**
     * 用户Id
     */
    private Long userId;
    /**
     * 请求url
     */
    private String url;
    /**
     * 请求方法
     */
    private String httpMethod;
}
