package com.sorcery.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 操作模式的枚举定义
 *
 * @author jinglv
 * @date 2024/4/29 16:44
 */
@Getter
@AllArgsConstructor
public enum OpModeEnum {
    READ("读"),
    WRITE("写");

    private final String mode;
}
