package com.sorcery.coupon.controller;

import com.sorcery.coupon.annotation.IgnoreResponseAdvice;
import com.sorcery.coupon.service.PathService;
import com.sorcery.coupon.service.PermissionService;
import com.sorcery.coupon.vo.CheckPermissionRequest;
import com.sorcery.coupon.vo.CreatePathRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 路径创建与权限校验对外服务接口实现
 *
 * @author jinglv
 * @date 2024/4/30 16:12
 */
@Slf4j
@RequiredArgsConstructor
@RestController
public class PermissionController {

    private final PathService pathService;
    private final PermissionService permissionService;

    /**
     * 路径创建接口
     *
     * @param createPathRequest 路径创建请求实体
     * @return 接口返回
     */
    @PostMapping("/create/path")
    public List<Integer> createPath(@RequestBody CreatePathRequest createPathRequest) {
        log.info("createPath: {}", createPathRequest.getPathInfoList().size());
        return pathService.createPath(createPathRequest);
    }

    /**
     * 权限校验接口
     *
     * @param checkPermissionRequest 权限校验请求实体
     * @return 接口返回
     */
    @IgnoreResponseAdvice
    @PostMapping("/check/permission")
    public Boolean checkPermission(@RequestBody CheckPermissionRequest checkPermissionRequest) {
        log.info("checkPermission for args: {}, {}, {}", checkPermissionRequest.getUserId(), checkPermissionRequest.getUrl(), checkPermissionRequest.getHttpMethod());
        return permissionService.checkPermission(checkPermissionRequest.getUserId(), checkPermissionRequest.getUrl(), checkPermissionRequest.getHttpMethod());
    }
}
