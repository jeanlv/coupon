package com.sorcery.coupon.dao;

import com.sorcery.coupon.entity.UserRoleMapping;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 用户（User）与角色（Role）的映射关系表DAO接口
 *
 * @author jinglv
 * @date 2024/4/30 11:09
 */
public interface UserRoleMappingRepository extends JpaRepository<UserRoleMapping, Long> {
    /**
     * 通过userId查询数据记录
     *
     * @param userId 用户id
     * @return 数据记录
     */
    UserRoleMapping findByUserId(Long userId);
}
