package com.sorcery.coupon.dao;

import com.sorcery.coupon.entity.RolePathMapping;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 角色（Role）与路径（Path）的映射关系表DAO接口
 *
 * @author jinglv
 * @date 2024/4/30 11:06
 */
public interface RolePathMappingRepository extends JpaRepository<RolePathMapping, Integer> {
    /**
     * 通过角色id + 路径id查找映射关系记录
     *
     * @param roleId 角色id
     * @param pathId 路径id
     * @return 映射关系记录
     */
    RolePathMapping findByRoleIdAndPathId(Integer roleId, Integer pathId);
}
