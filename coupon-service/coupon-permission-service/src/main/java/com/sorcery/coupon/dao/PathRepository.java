package com.sorcery.coupon.dao;

import com.sorcery.coupon.entity.Path;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Path表对应的DAO接口
 *
 * @author jinglv
 * @date 2024/4/30 11:00
 */
public interface PathRepository extends JpaRepository<Path, Integer> {
    /**
     * 根据服务名称查找Path记录
     *
     * @param serviceName 服务名称
     * @return Path记录
     */
    List<Path> findAllByServiceName(String serviceName);

    /**
     * 根据 路径模式+请求类型 查找Path记录
     *
     * @param pathPattern 路径模式
     * @param httpMethod  请求类型
     * @return Path记录
     */
    Path findByPathPatternAndHttpMethod(String pathPattern, String httpMethod);
}
