package com.sorcery.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 权限服务启动程序
 *
 * @author jinglv
 * @date 2024/4/29 16:57
 */
@EnableEurekaClient
@SpringBootApplication
public class CouponPermissionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CouponPermissionServiceApplication.class, args);
    }

}