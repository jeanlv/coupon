package com.sorcery.coupon.dao;

import com.sorcery.coupon.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Role表对应的DAO接口
 *
 * @author jinglv
 * @date 2024/4/30 11:05
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
