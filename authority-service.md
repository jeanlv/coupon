# 设计权限系统

- 权限系统核心思想

  - 用户：权限是针对不同的用户设定不同的访问范围
  - 可读、可写：权限是针对不同的用户对某个接口有可读、可写权限
  - （Controller接口）：权限是针对外服务的接口的，即Controller

- 权限系统工作方式

  <img src="https://jing-images.oss-cn-beijing.aliyuncs.com/img/202402261116772.png" alt="image-20240226111643659" style="zoom:50%;" />

- 权限系统的实现
  - permission-detector权限探测器
    - 其他微服务引用，能够自主探测微服务中定义的接口权限
  - permission-sdk权限常量、接口SDK
    - 权限常量、请求对象、Feign接口定义
  - permission-service权限（HTTP）服务
    - 权限微服务，提供权限的两个核心服务：权限创建、权限校验



# 权限系统数据表

- 权限系统所涉及的数据表

  - 用户角色表：coupon_role

    | 字段      | 含义             |
    | --------- | ---------------- |
    | id        | 自增主键，角色id |
    | role_name | 角色名称         |
    | role_tag  | 角色TAG标识      |

    SQL:

    ```sql
    -- 创建用户角色表
    CREATE TABLE IF NOT EXISTS `coupon_data`.`coupon_role` (
      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID, 自增主键',
      `role_name` varchar(128) NOT NULL DEFAULT '' COMMENT '角色名称',
      `role_tag` varchar(128) NOT NULL DEFAULT '' COMMENT '角色TAG标识',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户角色表';
    ```

    

  - 路径信息表：coupon_path

    | 字段         | 含义                      |
    | ------------ | ------------------------- |
    | id           | 自增主键，路径id          |
    | path_pattern | 路径模式(url)             |
    | http_method  | http请求类型(GET、POST等) |
    | path_name    | 路径描述                  |
    | service_name | 服务名称                  |
    | op_mode      | 操作类型，READ/WRITE      |

    SQL:

    ```sql
    -- 创建路径信息表
    CREATE TABLE IF NOT EXISTS `coupon_data`.`coupon_path` (
      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路径ID, 自增主键',
      `path_pattern` varchar(200) NOT NULL DEFAULT '' COMMENT '路径模式	',
      `http_method` varchar(20) NOT NULL DEFAULT '' COMMENT 'http请求类型',
      `path_name` varchar(50) NOT NULL DEFAULT '' COMMENT '路径描述',
      `service_name` varchar(50) NOT NULL DEFAULT '' COMMENT '服务名',
      `op_mode` varchar(20) NOT NULL DEFAULT '' COMMENT '操作类型, READ/WRITE',
      PRIMARY KEY (`id`),
      KEY `idx_path_pattern` (`path_pattern`),
      KEY `idx_service_name` (`service_name`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='路径信息表';
    ```

    

  - 角色路径映射表：coupon_role_path_mapping

    | 字段    | 含义     |
    | ------- | -------- |
    | id      | 自增主键 |
    | role_id | 角色id   |
    | path_id | 路径id   |

    SQL:

    ```sql
    -- 创建 Role 与 Path 的映射关系表
    CREATE TABLE IF NOT EXISTS `coupon_data`.`coupon_role_path_mapping` (
      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
      `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
      `path_id` int(11) NOT NULL DEFAULT '0' COMMENT '路径ID',
      PRIMARY KEY (`id`),
      KEY `idx_role_id` (`role_id`),
      KEY `idx_path_id` (`path_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='角色路径映射表';
    ```

    

  - 用户角色关系映射表：coupon_user_role_mapping

    | 字段    | 含义     |
    | ------- | -------- |
    | id      | 自增主键 |
    | user_id | 用户id   |
    | role_id | 角色id   |

    SQL：

    ```sql
    -- 创建 User 与 Role 的映射关系表
    CREATE TABLE IF NOT EXISTS `coupon_data`.`coupon_user_role_mapping` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
      `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
      `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
      PRIMARY KEY (`id`),
      KEY `key_role_id` (`role_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户角色关系映射表';
    ```

    

- 权限系统数据表工作关系

  <img src="https://jing-images.oss-cn-beijing.aliyuncs.com/img/202402261553876.png" alt="image-20240226155355728" style="zoom:50%;" />



